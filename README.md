Make sure you have Java 8 installed

- In the root folder

mvnw clean install
mvnw spring-boot:run

-This will start the server on port 8080

- Also, we have [topic/response] as a websocket topic where our Angular UI will be subscribed
- We have one endpoint [/hit-me] which will do some processing and once done, we can print anything on UI 