package com.db.socvice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocviceApplication.class, args);
	}

}
