package com.db.socvice.controller;

import java.time.LocalTime;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

@CrossOrigin("*")
@RestController
@RequestMapping("/ep")
public class RestSocket {
	
	@Autowired
	MessageSendingOperations<String> messageSendingOperations;
	
	@GetMapping("/trigger-api")
	@SendTo("/topic/response")
	public String greetingasd() throws Exception {
		Thread.sleep(1000);
		System.out.println("received asdasd asd ");
		return new String("Hello");
	}

	@Scheduled(fixedDelay = 1000)
	@GetMapping("/trigger-scheduled")
	public void sendPeriodicMessagesScheduled(String message) {
		System.out.println("received scheduled");
		String broadcast = String.format("server periodic message %s via the broker " + message, LocalTime.now());
		this.messageSendingOperations.convertAndSend("/topic/scheduled", broadcast);
	}
	
	@GetMapping("/trigger-scheduled-api")
	public void greeting(@PathParam("message") String message) throws Exception {
		System.out.println("received asd");
		int i = 0;
		while (i < 40) {
			Thread.sleep(1000);
			i++;
			messageSendingOperations.convertAndSend("/topic/scheduled",
					new String("Hello, " + HtmlUtils.htmlEscape(message) + "!"));
		}
	}

}
