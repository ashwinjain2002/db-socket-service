package com.db.socvice.controller;

import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

@CrossOrigin("*")
@RestController
public class MessagingController {

	@Autowired
	MessageSendingOperations<String> messageSendingOperations;

	@MessageMapping("/trigger")
	@SendTo("/topic/response")
	public void greeting(String message) throws Exception {
		System.out.println("received asd");
		int i = 0;
		while (i < 40) {
			Thread.sleep(1000);
			i++;
			messageSendingOperations.convertAndSend("/topic/response",
					new String("Hello, " + HtmlUtils.htmlEscape(message) + "!"));
		}
	}
}
